package com.investree.backendtest.controllers;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

// import com.investree.backendtest.models.Weather;
@RestController
public class WeatherController
{
 @GetMapping(value = "/weatherTest", produces = "application/json; charset=utf-8")
 public String getWeatherTest()
 {
  return "{ \"isWorking\" : true }";
 }
 
 @GetMapping("/weatherCity/{search}")
 public ResponseEntity<String> searchCityWeather(@PathVariable String search)
 {
    final String uri = "https://www.metaweather.com/api/location/search/?query="+search;
     
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
      return result; 
 }
}

@GetMapping("/weatherLatLong/{search}")
 public ResponseEntity<String> searchCityLatLong(@PathVariable String search)
 {
    final String uri = "https://www.metaweather.com/api/location/search/?lattlong="+search;
     
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
      return result; 
 }
}