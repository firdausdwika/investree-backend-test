package com.investree.backendtest.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.Accessors;
@AllArgsConstructor
@Accessors(fluent=true) @Getter
public class Weather {
 @JsonProperty
 private @NonNull Integer id;
 
 @JsonProperty
 private String title;
 
 @JsonProperty
 private String location_type;

 @JsonProperty
 private String woeid;

 @JsonProperty
 private String latLong;
}